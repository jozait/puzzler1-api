#!/bin/bash
# Delete all containers
docker rm $(docker ps -a -q1)
# Delete all images
docker rmi $(docker images -q)
