package fr.joza.poza.puzzler1.puzzler1api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Puzzler1ApiApplicationTests {

	@Test
	public void contextLoads() {
	}

}
