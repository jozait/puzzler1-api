package fr.joza.poza.puzzler1.puzzler1api.web.rest;

import fr.joza.poza.puzzler1.puzzler1api.Puzzler1ApiApplication;
import fr.joza.poza.puzzler1.puzzler1api.domain.Customer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Puzzler1ApiApplication.class)
public class CustomerControllerTest {

    @Autowired
    private CustomerController controller;

    @Test
    public void test01_GIVEN_a_new_WHEN_save_THEN_api_returns_CREATED() {
        ResponseEntity<Customer> response = controller.save(new Customer("monlogin", "monadresse@doamin.com", "pwdPuzzl3r!"));

        Assert.assertNotNull(response);
        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        Assert.assertNotNull(response.getBody());
    }

 // Exercice 1
    @Test
    public void test02_GIVEN_customer_with_too_short_login_WHEN_save_THEN_api_returns_ERRORS() {
        ResponseEntity<Customer> response = controller.save(new Customer("mon", "monadresse@doamin.com", "pwdPuzzl3r!"));

        Assert.assertNotNull(response);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assert.assertNull(response.getBody());
    }

    // Exercice 2
    @Test
    public void test03_GIVEN_password_too_short_login_WHEN_save_THEN_api_returns_ERRORS() {
        ResponseEntity<Customer> response = controller.save(new Customer("monlogin", "monadresse@doamin.com", "p3R!"));

        Assert.assertNotNull(response);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assert.assertNull(response.getBody());
    }

    @Test
    public void test04_GIVEN_password_without_digit_WHEN_save_THEN_api_returns_ERRORS() {
        ResponseEntity<Customer> response = controller.save(new Customer("monlogin", "monadresse@doamin.com", "pazertyuiR!"));

        Assert.assertNotNull(response);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assert.assertNull(response.getBody());
    }

    @Test
    public void test05_GIVEN_password_without_special_char_WHEN_save_THEN_api_returns_ERRORS() {
        ResponseEntity<Customer> response = controller.save(new Customer("monlogin", "monadresse@doamin.com", "pazertyuiR5"));

        Assert.assertNotNull(response);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assert.assertNull(response.getBody());
    }

}
