package fr.joza.poza.puzzler1.puzzler1api.repository;

import fr.joza.poza.puzzler1.puzzler1api.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
