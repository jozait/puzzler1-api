package fr.joza.poza.puzzler1.puzzler1api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Puzzler1ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Puzzler1ApiApplication.class, args);
	}

}
