package fr.joza.poza.puzzler1.puzzler1api.web.rest;

import fr.joza.poza.puzzler1.puzzler1api.domain.Customer;
import fr.joza.poza.puzzler1.puzzler1api.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("${api.base.url}/customers")
public class CustomerController {

    @Autowired
    private CustomerRepository repository;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Customer> getById(Long id) {
        Optional<Customer> customer = repository.findById(id);

        if (customer.isPresent()) {
            return new ResponseEntity<>(customer.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Customer> save(@RequestBody Customer customer) {
        customer = repository.save(customer);
        return new ResponseEntity<>(customer, HttpStatus.CREATED);
    }
}
