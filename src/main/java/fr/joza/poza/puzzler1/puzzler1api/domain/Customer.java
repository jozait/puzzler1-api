package fr.joza.poza.puzzler1.puzzler1api.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public String login;

    public String email;

    public String password;

    public Customer() {
    }

    public Customer(String login, String email, String password) {
        this.login = login;
        this.email = email;
        this.password = password;
    }
}
