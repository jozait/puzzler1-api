create table customer(
  id bigserial constraint pk_customer primary key,
  login text not null check (char_length(login) = 8),
  email text not null,
  password text not null check (char_length(password) >= 6)
);