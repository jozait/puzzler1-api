# Joza Puzzler 1

# Installation & démarrage du projet

## pré-requis

Maven (ou intellij)
Java 8
Postgres (ou Docker & Docker compose)

## base de données dockerisé

Elle démarre sur le port 15432

A la racine du projet
```
docker-compose up
```

## base de données locale

### création du user et de la base postgres

Connectez vous avez votre console psql en local

```
CREATE USER jozapuzzler1 WITH PASSWORD 'jozapuzzler1';
CREATE DATABASE jozapuzzler1;
\connect jozapuzzler1;
create schema jozapuzzler1 authorization jozapuzzler1;
GRANT ALL PRIVILEGES ON DATABASE jozapuzzler1 to jozapuzzler1;
```
(changer le port dans le fichier application.properties)

## application spring boot

### dans votre IDE

Lancer la classe Puzzler1ApiApplication en tant qu'application Java

### en ligne de commande

A la racine du projet
```
mvn clean install -Dmaven.test.skip=true
java -jar target/puzzler1-api-0.0.1-SNAPSHOT.jar
```

## Vérification de l'installation

Lancer swagger en local
```
http://localhost:8080/swagger-ui.html
```